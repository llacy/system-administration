#!/bin/bash
ISO_PATH=/home/libvirt/ISO

read -p "Machine name [ol84-gold]:" NAME
NAME=${NAME:-ol84-gold}
echo NAME: $NAME
echo ""

read -p "CPUs needed [2]: " CPUS
CPUS=${CPUS:-2}
echo CPUS: $CPUS
echo ""

read -p "Memory needed [2048]: " RAM
RAM=${RAM:-2048}
echo RAM: $RAM
echo ""

ls -c1 /home/libvirt/ISO | grep -v cfg
echo ""
read -p "ISO [OracleLinux-R8-U4-x86_64-dvd.iso]:" ISO
ISO=${ISO:-OracleLinux-R8-U4-x86_64-dvd.iso}
echo ISO: $ISO
echo ""

read -p "OS Type [linux]: " TYPE
TYPE=${TYPE:-linux}
echo TYPE: $TYPE
echo ""

read -p "Do you need a list of varriants [n]: " LIST
LIST=${LIST:-n}
echo LIST: $LIST
echo ""

if [ $LIST == "y" ]; then
	osinfo-query --fields=short-id,name os family="$TYPE"
	echo ""
fi

read -p "OS Varriant [ol8.2]:" VARRIANT
VARRIANT=${VARRIANT:-ol8.2}
echo VARRIANT: $VARRIANT
echo ""

read -p "GUI [n]:" GUI
GUI=${GUI:-n}
echo GUI: $GUI
echo ""

if [ $GUI == "y" ]; then
	KSFILE=gui-ks.cfg
else
	KSFILE=nogui-ks.cfg
fi

sudo virt-install \
	--name "$NAME" \
	--ram "$RAM" \
	--vcpus "$CPUS" \
	--os-type "$TYPE" \
	--os-variant "$VARRIANT" \
	--location "$ISO_PATH/$ISO" \
	--disk path="/home/libvirt/images/$NAME.qcow2,size=20" \
	--network bridge="virbr0" \
	--initrd-inject="/home/libvirt/ISO/$KSFILE" \
	--extra-args "inst.ks=file:/$KSFILE" \
	--noautoconsole \
	--wait
#	--graphics "spice" \
#	--description "$DESCRIPTION" \

