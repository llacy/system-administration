#!/bin/bash

# This script generates a password and hash for each day of the year and outputs
# them to seperate files as defined by HASHFILE and PWFILE.

HASHFILE=/root/HASH
PWFILE=//tmp/PW

# Remove old password and hash table
if [ -f $HASHFILE ]; then
	sudo rm $HASHFILE
fi

if [ -f $PWFILE ]; then
	sudo rm $PWFILE
fi

# Generate new password and hash table
for i in {1..365}; do
	RDM=$(</dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-10};echo;)
#	echo $RDM
	PW=$(echo $RANDOM | md5sum | awk '{print $1}')
	echo "$i $PW" | sudo tee -a $PWFILE;
	HASH=$(openssl passwd -6 -salt $RDM $PW);
	echo $i $HASH | sudo tee -a $HASHFILE;
done

